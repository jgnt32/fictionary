package ru.timewaistingguru.fictionary.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Locale;

import ru.timewaistingguru.fictionary.R;
import ru.timewaistingguru.fictionary.adapters.LanguageSpinnerAdapter;
import ru.timewaistingguru.fictionary.adapters.OriginalLanguageSpinnerAdapter;
import ru.timewaistingguru.fictionary.database.DataManager;
import ru.timewaistingguru.fictionary.database.PreferenceManager;
import ru.timewaistingguru.fictionary.entities.Word;
import ru.timewaistingguru.fictionary.rest.RestManager;
import ru.timewaistingguru.fictionary.widgets.PostSubmitEditText;
import rx.android.app.AppObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by artoymtkachenko on 12.03.15.
 */
public class TranslateFragment extends DialogFragment implements PostSubmitEditText.OnPostSubmitListener {

    private PostSubmitEditText mEditText;
    private TextView mTranslate;
    private Word mWord;

    private Spinner mOriginalLaunguage;
    private Spinner mTranslateLangeage;

    private ImageButton mImageButton;
    private ProgressBar mProgressBar;
    private LanguageSpinnerAdapter mTranslateAdapter;
    private OriginalLanguageSpinnerAdapter mOriginalAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.add_word);
        View v = inflater.inflate(R.layout.translate_fragment, container);

        mEditText = (PostSubmitEditText) v.findViewById(R.id.edit_text);
        mEditText.setOnPostSubmitListener(this);
        mTranslate = (TextView) v.findViewById(R.id.translated_word);

        v.findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mWord != null) {
                    DataManager.getInstance().saveWord(mWord);
                    dismiss();
                }
            }
        });

        v.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mOriginalLaunguage = (Spinner) v.findViewById(R.id.original_language_spinner);
        mOriginalAdapter = new OriginalLanguageSpinnerAdapter(getActivity());
        mOriginalLaunguage.setAdapter(mOriginalAdapter);

        int positionByLangCode1 = mOriginalAdapter.getPositionByLangCode(PreferenceManager.getInstance().getDefaulOriginalLanguage());
        mOriginalLaunguage.setSelection(positionByLangCode1);

        mOriginalLaunguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PreferenceManager.getInstance().saveDefaultOriginalLanguage(mOriginalAdapter.getLangCode(position));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mTranslateLangeage = (Spinner) v.findViewById(R.id.translate_language_spinner);
        mTranslateAdapter = new LanguageSpinnerAdapter(getActivity());
        mTranslateLangeage.setAdapter(mTranslateAdapter);

        String defaulTraslateLanguage = PreferenceManager.getInstance().getDefaulTraslateLanguage();

        if (defaulTraslateLanguage == null) {
            Locale locale = Locale.getDefault();
            defaulTraslateLanguage = locale.getLanguage();
        }

        int positionByLangCode = mTranslateAdapter.getPositionByLangCode(defaulTraslateLanguage);
        mTranslateLangeage.setSelection(positionByLangCode);

        mTranslateLangeage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PreferenceManager.getInstance().saveDefaultTraslateLanguage(mTranslateAdapter.getLangCode(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mProgressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        mImageButton = (ImageButton) v.findViewById(R.id.clear_button);
        mImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditText.setText("");
            }
        });
        return v;
    }


    @Override
    public void onPostSubmit(String text) {
        showProgress();

        AppObservable.bindFragment(TranslateFragment.this, RestManager.getInstatnce().translate(mEditText.getText().toString(), getOriginalLangCode(), getTargetLangCode()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, Word>() {
                    @Override
                    public Word call(Throwable throwable) {
                        Toast.makeText(getActivity(), R.string.error, Toast.LENGTH_SHORT).show();
                        return null;
                    }
                })
                .subscribe(new Action1<Word>() {
                    @Override
                    public void call(Word word) {
                        if (word != null && word.getTranslate() != null) {
                            Gson gson = new Gson();
                            String[] translate = gson.fromJson(word.getTranslate(), String[].class);
                            if (translate.length > 0) {
                                mTranslate.setText(translate[0]);
                                mWord = word;
                                mWord.setOriginal(mEditText.getText().toString());
                                mOriginalLaunguage.setSelection(mOriginalAdapter.getPositionByLangCode(extractOriginalLangCode(mWord.getLang())));

                            }
                        }
                        hideProgress();

                    }
                });
    }

    private String extractOriginalLangCode(String pairLangCode) {
        int divider = pairLangCode.indexOf("-");
        return pairLangCode.substring(0, divider);
    }


    private String getOriginalLangCode() {
        return mOriginalAdapter.getLangCode(mOriginalLaunguage.getSelectedItemPosition());
    }

    private String getTargetLangCode() {
        return mTranslateAdapter.getLangCode(mTranslateLangeage.getSelectedItemPosition());
    }

    private void showProgress() {
        mImageButton.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }


    private void hideProgress() {
        mImageButton.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onTextChange(String text) {
    }
}
