package ru.timewaistingguru.fictionary.database;

import android.content.SharedPreferences;

import ru.timewaistingguru.fictionary.App;

/**
 * Created by artoymtkachenko on 15.03.15.
 */
public class PreferenceManager {


    public static final String ORIGNAL_LANG_KEY = "original_lang_key";
    public static final String TRANSLATE_LANG_KEY = "translate_lang_key";

    private static PreferenceManager ourInstance = new PreferenceManager();
    private SharedPreferences mSharedPreferences;

    private PreferenceManager() {
        mSharedPreferences = android.preference.PreferenceManager.getDefaultSharedPreferences(App.getContext());
    }

    public static PreferenceManager getInstance() {
        return ourInstance;
    }

    public void saveDefaultOriginalLanguage(String languageCode) {
        mSharedPreferences.edit().putString(ORIGNAL_LANG_KEY, languageCode).commit();
    }

    public void saveDefaultTraslateLanguage(String languageCode) {
        mSharedPreferences.edit().putString(TRANSLATE_LANG_KEY, languageCode).commit();

    }

    public String getDefaulOriginalLanguage() {
        return mSharedPreferences.getString(ORIGNAL_LANG_KEY, null);
    }

    public String getDefaulTraslateLanguage() {
        return mSharedPreferences.getString(TRANSLATE_LANG_KEY, null);
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

}
