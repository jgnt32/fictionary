package ru.timewaistingguru.fictionary.database;

import android.content.Context;
import android.util.Log;

import io.realm.Realm;
import ru.timewaistingguru.fictionary.App;
import ru.timewaistingguru.fictionary.entities.Word;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by artoymtkachenko on 13.03.15.
 */
public class DataManager {

    private static DataManager ourInstance = new DataManager();
    private Context mContext = App.getContext();

    private DataManager() {
    }

    public static DataManager getInstance() {
        return ourInstance;
    }

    public void saveWord(final Word word) {

        Observable.create(new Observable.OnSubscribe<Word>() {

            @Override
            public void call(Subscriber<? super Word> subscriber) {
                subscriber.onNext(word);
                subscriber.onCompleted();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<Word>() {
                    @Override
                    public void call(Word word) {
                        Realm realm = Realm.getInstance(mContext);
                        realm.beginTransaction();
                        Word realmWord = realm.copyToRealmOrUpdate(word);
                        realm.commitTransaction();
                        realm.close();
                    }
                })
                .subscribe(new Action1<Word>() {
                    @Override
                    public void call(Word word) {
                        Log.e("translate", "" + word);

                    }
                });
    }


}
