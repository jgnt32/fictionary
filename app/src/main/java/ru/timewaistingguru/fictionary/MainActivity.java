package ru.timewaistingguru.fictionary;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import ru.timewaistingguru.fictionary.adapters.LanguageFilterAdapter;
import ru.timewaistingguru.fictionary.adapters.WordAdapter;
import ru.timewaistingguru.fictionary.database.PreferenceManager;
import ru.timewaistingguru.fictionary.entities.Word;
import ru.timewaistingguru.fictionary.fragments.TranslateFragment;
import ru.timewaistingguru.fictionary.widgets.FloatingActionButton;
import ru.timewaistingguru.fictionary.widgets.FreeRecyclerView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private WordAdapter mWordAdapter;
    private FreeRecyclerView mRecyclerView;
    private RealmResults<Word> mWords;
    private Realm mRealm;
    private Spinner mOriginalLaunguage;
    private LanguageFilterAdapter mOriginalAdapter;
    private Spinner mTranslateLangeage;
    private LanguageFilterAdapter mTranslateAdapter;
    private SearchView mSearchView;
    private String mQueryFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        mRecyclerView = (FreeRecyclerView) findViewById(R.id.word_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mRecyclerView.setEmptyView(findViewById(R.id.empty_view));

        mRealm = Realm.getInstance(this);
        mRealm.addChangeListener(new RealmChangeListener() {
            @Override
            public void onChange() {
                mWordAdapter.notifyDataSetChanged();
            }
        });

        FloatingActionButton fabButton = new FloatingActionButton.Builder(this)
                .withDrawable(getResources().getDrawable(R.drawable.plus))
                .withButtonColor(getResources().getColor(R.color.red))
                .withGravity(Gravity.BOTTOM | Gravity.RIGHT)
                .withMargins(0, 0, 16, 16)
                .create();

        fabButton.setOnClickListener(this);


        mOriginalLaunguage = (Spinner) findViewById(R.id.original_language_spinner);
        mOriginalAdapter = new LanguageFilterAdapter(this);
        mOriginalLaunguage.setAdapter(mOriginalAdapter);


        mOriginalLaunguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PreferenceManager.getInstance().saveDefaultOriginalLanguage(mOriginalAdapter.getLangCode(position));
                refreshList();
                mWordAdapter.swapWords(mWords);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mTranslateLangeage = (Spinner) findViewById(R.id.translate_language_spinner);
        mTranslateAdapter = new LanguageFilterAdapter(this);
        mTranslateLangeage.setAdapter(mTranslateAdapter);


        mTranslateLangeage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PreferenceManager.getInstance().saveDefaultTraslateLanguage(mTranslateAdapter.getLangCode(position));
                refreshList();
                mWordAdapter.swapWords(mWords);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        refreshTargetSpinner();

        refreshOriginalSpinner();

        refreshList();

        mWordAdapter = new WordAdapter(mWords, this);
        mRecyclerView.setAdapter(mWordAdapter);


        PreferenceManager.getInstance().registerOnSharedPreferenceChangeListener(this);

    }

    private void refreshOriginalSpinner() {
        int positionByLangCode1 = mOriginalAdapter.getPositionByLangCode(PreferenceManager.getInstance().getDefaulOriginalLanguage());
        mOriginalLaunguage.setSelection(positionByLangCode1);
    }

    private void refreshTargetSpinner() {
        int positionByLangCode = mTranslateAdapter.getPositionByLangCode(PreferenceManager.getInstance().getDefaulTraslateLanguage());
        mTranslateLangeage.setSelection(positionByLangCode);
    }


    private void refreshList() {
        String originalLangCode = mOriginalAdapter.getLangCode(mOriginalLaunguage.getSelectedItemPosition());
        String translateLangCode = mTranslateAdapter.getLangCode(mTranslateLangeage.getSelectedItemPosition());
        mWords = getWords(originalLangCode, translateLangCode, mQueryFilter);
    }


    public RealmResults<Word> getWords(String originalLang, String targetLang, String filter) {
        RealmQuery<Word> query = mRealm.where(Word.class);

        if (originalLang != null && targetLang == null) {
            query = query.beginsWith(Word.LANG, originalLang);

        } else if (targetLang != null && originalLang == null) {

            query = query.endsWith(Word.LANG, targetLang);
        } else if (originalLang != null && targetLang != null) {
            String lang = TextUtils.concat(originalLang, "-", targetLang).toString();
            query = query.equalTo(Word.LANG, lang);
        }

        if (filter != null && !filter.isEmpty()) {
            query = query.contains(Word.ORIGINAL, filter).or().contains(Word.TRANSLATE, filter);
        }

        RealmResults<Word> result = query.findAll();
        result.sort(Word.ORIGINAL);
        return result;

    }


    @Override
    protected void onDestroy() {
        PreferenceManager.getInstance().unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
        mRealm.close();
    }

    @Override
    public void onClick(View v) {
        TranslateFragment fragment = new TranslateFragment();
        fragment.show(getFragmentManager(), null);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equalsIgnoreCase(PreferenceManager.ORIGNAL_LANG_KEY)) {


            refreshOriginalSpinner();

        } else if (key.equalsIgnoreCase(PreferenceManager.TRANSLATE_LANG_KEY)) {
            refreshTargetSpinner();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) item.getActionView();
        // mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                mQueryFilter = s;
                refreshList();
                mWordAdapter.swapWords(mWords);
                return false;
            }
        });

        AutoCompleteTextView searchText = (AutoCompleteTextView) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setHint(R.string.search_hint);
        return true;
    }


}
