package ru.timewaistingguru.fictionary.widgets;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by artoymtkachenko on 12.03.15.
 */
public class PostSubmitEditText extends EditText implements TextWatcher {

    public static int DEFAULT_INTERVAL = 500;
    private PostSubmitTimer mPostSubmitTimer = new PostSubmitTimer(DEFAULT_INTERVAL, 1);
    private OnPostSubmitListener mOnPostSubmitListener;

    public PostSubmitEditText(Context context) {
        super(context);
        init();
    }


    public PostSubmitEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PostSubmitEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mOnPostSubmitListener != null) {
            mOnPostSubmitListener.onTextChange(s.toString());
            mPostSubmitTimer.cancel();
            mPostSubmitTimer.start();
        }

    }

    public OnPostSubmitListener getOnPostSubmitListener() {
        return mOnPostSubmitListener;
    }

    public void setOnPostSubmitListener(OnPostSubmitListener mOnPostSubmitListener) {
        this.mOnPostSubmitListener = mOnPostSubmitListener;
    }

    public interface OnPostSubmitListener {
        public void onPostSubmit(String text);

        public void onTextChange(String text);
    }

    private class PostSubmitTimer extends CountDownTimer {


        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public PostSubmitTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {

            if (mOnPostSubmitListener != null && !getText().toString().isEmpty()) {
                mOnPostSubmitListener.onPostSubmit(getText().toString());
            }

        }
    }

}
