package ru.timewaistingguru.fictionary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import io.realm.RealmResults;
import ru.timewaistingguru.fictionary.R;
import ru.timewaistingguru.fictionary.entities.Word;

/**
 * Created by artoymtkachenko on 12.03.15.
 */
public class WordAdapter extends RecyclerView.Adapter<WordAdapter.WordHeader> {


    private RealmResults<Word> mWords;
    private Context mContext;
    private Gson gson = new Gson();


    public WordAdapter(RealmResults<Word> mWords, Context mContext) {
        this.mWords = mWords;
        this.mContext = mContext;
    }


    @Override
    public WordAdapter.WordHeader onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.word_list_item, viewGroup, false);
        return new WordHeader(v);
    }


    @Override
    public void onBindViewHolder(WordAdapter.WordHeader viewHolder, int i) {
        viewHolder.original.setText(mWords.get(i).getOriginal());
        String[] translates = gson.fromJson(mWords.get(i).getTranslate(), String[].class);
        String translatedWord = "";
        if (translates != null && translates.length > 0) {
            translatedWord = translates[0];
        }
        viewHolder.trasnslate.setText(translatedWord);
    }

    @Override
    public int getItemCount() {
        return mWords.size();
    }

    public void swapWords(RealmResults<Word> words) {
        mWords = words;
        notifyDataSetChanged();
    }


    public class WordHeader extends RecyclerView.ViewHolder {

        TextView original;
        TextView trasnslate;

        public WordHeader(View itemView) {
            super(itemView);
            original = (TextView) itemView.findViewById(R.id.original);
            trasnslate = (TextView) itemView.findViewById(R.id.translate);
        }
    }
}
