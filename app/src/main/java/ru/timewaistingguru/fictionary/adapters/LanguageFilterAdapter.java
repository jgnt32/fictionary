package ru.timewaistingguru.fictionary.adapters;

import android.content.Context;

import ru.timewaistingguru.fictionary.R;

/**
 * Created by artoymtkachenko on 15.03.15.
 */
public class LanguageFilterAdapter extends LanguageSpinnerAdapter {

    public LanguageFilterAdapter(Context context) {
        super(context);
    }

    @Override
    public String getItem(int position) {
        if (position == 0) {
            return mContext.getResources().getString(R.string.any_language);
        } else {
            return super.getItem(position - 1);
        }
    }

    @Override
    public String getLangCode(int position) {
        String result = null;
        if (position > 0) {
            result = super.getLangCode(position - 1);
        }
        return result;

    }

    @Override
    public int getCount() {
        return super.getCount() + 1;
    }

    @Override
    public int getPositionByLangCode(String langCode) {
        int result = 0;
        if (langCode != null) {
            for (int i = 0; i < mLocales.length; i++) {
                if (mLocales[i].equalsIgnoreCase(langCode)) {
                    result = i + 1;
                }
            }
        }

        return result;
    }


}
