package ru.timewaistingguru.fictionary.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Locale;

import ru.timewaistingguru.fictionary.R;

/**
 * Created by artoymtkachenko on 15.03.15.
 */
public class LanguageSpinnerAdapter extends BaseAdapter {

    protected final Context mContext;
    protected final LayoutInflater mLayoutInflater;
    protected String[] mLocales = new String[]{};
    protected Holder mHolder;

    public LanguageSpinnerAdapter(Context context) {
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mLocales = mContext.getResources().getStringArray(R.array.locales);
    }

    @Override
    public int getCount() {
        return mLocales.length;
    }

    @Override
    public String getItem(int position) {
        Locale locale = new Locale(mLocales[position]);
        return locale.getDisplayLanguage();
    }

    public String getLangCode(int position) {
        String result = null;
        if (position >= 0 && position < mLocales.length) {
            result = mLocales[position];
        }
        return result;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.language_list_element, null);
            mHolder = new Holder(convertView);
            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }


        mHolder.language.setText(getItem(position));

        return convertView;
    }

    public int getPositionByLangCode(String langCode) {
        int result = 0;
        if (langCode != null) {
            for (int i = 0; i < mLocales.length; i++) {
                if (mLocales[i].equalsIgnoreCase(langCode)) {
                    result = i;
                }
            }
        }

        return result;
    }

    protected class Holder {

        private TextView language;

        public Holder(View itemView) {
            language = (TextView) itemView.findViewById(R.id.language_title);
        }
    }
}
