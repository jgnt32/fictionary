package ru.timewaistingguru.fictionary.entities;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by artoymtkachenko on 12.03.15.
 */
public class Word extends RealmObject {

    @Ignore
    public static final String LANG = "lang";
    @Ignore
    public static final String ORIGINAL = "original";
    @Ignore
    public static final String TRANSLATE = "translate";

    private String lang;

    @PrimaryKey
    private String original;

    @SerializedName("text")
    private String translate;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }
}
