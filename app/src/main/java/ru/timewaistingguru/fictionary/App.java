package ru.timewaistingguru.fictionary;

import android.app.Application;
import android.content.Context;

/**
 * Created by artoymtkachenko on 13.03.15.
 */
public class App extends Application {

    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }
}
