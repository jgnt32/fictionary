package ru.timewaistingguru.fictionary.rest;

import retrofit.http.GET;
import retrofit.http.Query;
import ru.timewaistingguru.fictionary.entities.Word;
import rx.Observable;

/**
 * Created by artoymtkachenko on 12.03.15.
 */
public interface YandexApi {

    @GET("/translate")
    public Observable<Word> translate(@Query("text") String text, @Query("lang") String lang);

}