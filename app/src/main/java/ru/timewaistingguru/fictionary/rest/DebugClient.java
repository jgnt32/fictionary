package ru.timewaistingguru.fictionary.rest;

import android.util.Log;

import java.io.IOException;

import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;

/**
 * Created by artoymtkachenko on 13.03.15.
 */
public class DebugClient implements Client {

    @Override
    public Response execute(Request request) throws IOException {
        Request signedRequest = new Request(
                request.getMethod(),
                request.getUrl(),
                request.getHeaders(),
                request.getBody());


        Client client = new OkClient();
        Log.e("url", "" + signedRequest.getUrl());

        return client.execute(signedRequest);
    }

}
