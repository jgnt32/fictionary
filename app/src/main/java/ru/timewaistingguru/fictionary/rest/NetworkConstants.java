package ru.timewaistingguru.fictionary.rest;

/**
 * Created by artoymtkachenko on 12.03.15.
 */
public class NetworkConstants {

    public static String ENDPOINT = "https://translate.yandex.net/api/v1.5/tr.json";

    public static String API_KEY = "trnsl.1.1.20150312T174937Z.e0b5e513f03c167d.8801338abfe2008a8b085b920cfc85cc146be896";

    public static String LANG = "lang";
    public static String KEY = "key";

}
