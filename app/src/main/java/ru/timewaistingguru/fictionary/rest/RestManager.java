package ru.timewaistingguru.fictionary.rest;


import android.content.Context;
import android.text.TextUtils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

import io.realm.RealmObject;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import ru.timewaistingguru.fictionary.App;
import ru.timewaistingguru.fictionary.entities.Word;
import rx.Observable;

/**
 * Created by artoymtkachenko on 12.03.15.
 */
public class RestManager {

    private static RestManager mInstatnce;
    private Context mContext;
    private YandexApi mYandexApi;

    public RestManager() {

        mContext = App.getContext();

        Gson gson = new GsonBuilder()
                .setExclusionStrategies(getExclusionStrategy())
                .registerTypeAdapter(String.class, new StringSerializer())
                .create();

        mYandexApi = new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setEndpoint(NetworkConstants.ENDPOINT)
                .setRequestInterceptor(getCustomInterceptor())
                .setClient(new DebugClient())
                .build()
                .create(YandexApi.class);

    }

    public static RestManager getInstatnce() {
        if (mInstatnce == null) {
            mInstatnce = new RestManager();
        }
        return mInstatnce;
    }

    public Observable<Word> translate(final String text, String originalLang, String targetLang) {
        String lang = targetLang;
        if (originalLang != null) {
            lang = TextUtils.concat(originalLang, "-", targetLang).toString();
        }
        return mYandexApi.translate(text, lang);

    }


    private RequestInterceptor getCustomInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                Map<String, String> map = getRequeredParams();

                for (Map.Entry<String, String> entries : map.entrySet()) {
                    request.addEncodedQueryParam(entries.getKey(), entries.getValue());
                }
            }
        };
    }


    private Map<String, String> getRequeredParams() {
        Map<String, String> result = new TreeMap<String, String>();
        result.put(NetworkConstants.KEY, NetworkConstants.API_KEY);
        return result;
    }

    private ExclusionStrategy getExclusionStrategy() {
        return new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        };
    }

    public class StringSerializer implements JsonDeserializer<String> {


        @Override
        public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            if (json.isJsonArray()) {
                Gson gson = new Gson();
                return gson.toJson(json);
            } else {
                return json.getAsString();
            }
        }

    }
}
